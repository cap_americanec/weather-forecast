import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../weather.service';
import { Chart } from 'chart.js'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  weatherService: WeatherService;
  city: string = 'London';
  dates = [];
  windArray = [];
  temperatureArray = [];
  pressureArray = [];
  humidityArray = [];
  chart: Chart = [];

  monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

  constructor(weatherService: WeatherService) {

    this.weatherService = weatherService;
   }

  ngOnInit() {
    this.weatherService.getWether(this.city)
      .subscribe((res) => {
        console.log(res);
        res.list.forEach(element => {
          console.log(element.dt)
          let date = new Date(element.dt_txt);
          console.log(date.getDay());
          this.dates.push(String(date.getDate() + ' ' + this.monthNames[date.getMonth()]));
          this.windArray.push(element.wind.speed);
          this.temperatureArray.push(element.main.temp);
          this.pressureArray.push(element.main.pressure);
          this.humidityArray.push(element.main.humidity);
        });
        this.chart = new Chart('canvas', {
          type: 'bar',
      data: {
        labels: this.dates,
        datasets: [
          {
            label: this.city,
            data: this.windArray,
            borderWidth: 1
          }
        ]
      },
      options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        },
    }
        });
        console.log(this.windArray);
        console.log(this.temperatureArray);
        console.log(this.pressureArray);
        console.log(this.humidityArray);
      }, err => {console.error(`Error ${err}`)})
  }

  changeCity(city){
    console.log(city);
    this.windArray = [];
    this.temperatureArray = [];
    this.pressureArray = [];
    this.humidityArray = [];

    this.weatherService.getWether(this.city)
      .subscribe((res) => {
        console.log(res);
        res.list.forEach(element => {
          this.windArray.push(element.wind.speed);
          this.temperatureArray.push(element.main.temp);
          this.pressureArray.push(element.main.pressure);
          this.humidityArray.push(element.main.humidity);
        });
        this.displayChart(this.windArray);
        console.log(this.windArray);
        console.log(this.temperatureArray);
        console.log(this.pressureArray);
        console.log(this.humidityArray);
      }, err => {console.error(`Error ${err}`)})
  }

  displayChart(inputArray){
    this.chart.destroy();
    this.chart = new Chart('canvas', {
      type: 'bar',
  data: {
    labels:  this.dates,
    datasets: [
      {
        label: this.city,
        data: inputArray,
        borderWidth: 1
      }
    ]
  },
  options: {
    scales: {
        yAxes: [{
            ticks: {
                beginAtZero:true
            }
        }]
    },
}
    });
  }

}
