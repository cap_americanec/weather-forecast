import { TestBed, inject } from '@angular/core/testing';

import { WetherService } from './wether.service';

describe('WetherService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WetherService]
    });
  });

  it('should be created', inject([WetherService], (service: WetherService) => {
    expect(service).toBeTruthy();
  }));
});
