import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class WeatherService {

  apiKey: string = '1c5830911a5f26ef856b81566817afed';
  url: string = '';


  constructor(private http: Http) {

    this.url = 'https:/api.openweathermap.org/data/2.5/forecast?q=';
   }

   getWether(cityName){

      console.log(cityName);
      return this.http.get(this.url+cityName+'&cnt=16'+'&APPID='+this.apiKey)
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Error getting adds'));
   }

}
